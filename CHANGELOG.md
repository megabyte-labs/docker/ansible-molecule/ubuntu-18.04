# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.3](https://gitlab.com/megabyte-labs/dockerfile/ansible-molecule/ubuntu-18.04/compare/v0.0.2...v0.0.3) (2021-06-17)

### 0.0.2 (2021-06-12)
